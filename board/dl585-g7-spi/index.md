# HP ProLiant DL580/DL585 G7 System Peripheral Interface Board

Part number: 591199-001

<!-- TODO: picture -->

This is a PCIe board that gives the DL580 G7 and DL585 G7 servers various
peripherals, such as a NIC, a SAS controller and an iLO 3.


## Chips

- iLO
  - [iLO3 GXE](../../soc/gxe/index.md)
  - Nanya [NT5TU64M16DG-AC], 1 Gbit DDR2 SDRAM
  - Atmel [AT25DF641-S3H]: SPI flash, SOIC-16, for iLO firmware
  - Cypress [CY62138FV30]: 2Mbit SRAM (probably battery-backed)
  - ALCOR [AU6336]: USB2.0 SD/MMC Single LUN Card Reader Controller
  - Pericom [PI3B3245LE]: 3.3V, Hot Insertion 8-Bit, 2-Port, NanoSwitch™
  - [ADT7461A]: ±1°C Temperature Monitor with Series Resistance Cancellation
  - [MAX8643A], labeled: 3A, 2MHz Step-Down Regulator with Integrated Switches
  - TI [SN74CBTLV3257]: Low-Voltage 4-Bit 1-of-2 FET Multiplexer/Demultiplexer
  - TI [SN74LVC244A]: Octal Buffer or Driver With 3-State Outputs
  - Maxim [MAX3243C]: +3V to +5.5V RS-232 Transceivers with AutoShutdown
  - BCM5241: 10/100 Mbit Ethernet PHY
- SAS
  - PMC SRC 8x6G PM8011C-F3GI: SAS controller
  - [MX 29LV640DBTC-90G]: 64Mbit flash
- Ethernet
  - NetXen PCIe Ethernet controller
  - Marvell [88E1240-BAM1] Ethernet PHY
  - [MX25L3205D]: 32Mbit NOR flash

## I/O ports

- one SD-Card slot, presumably for iLO
- VGA and serial, presumably via iLO
- two USB ports, presumably via iLO
- one Ethernet port (above the USB ports), for iLO
- PS/2 keyboard and mouse connectors, presumably via iLO
- two SAS ports, via a built-in SAS controller, with optional battery-backed
  memory
- four Ethernet ports, via the NetXen NIC, with built-in and optional memory
- one PCIe slot, for an optional 10G NIC

## Power

To run the card stand-alone, connect +12V to the `P12V_STBY` rail. Around 0.6A
will be drawn.

## Debug connectors

The card has a lot of unpopulated connectors with up to five pins spaced at 0.1
inch:

| Ref. | pin 1 | pin 2 | pin 3 | pin 4 |  Function, comments                  |
| ---- | ----- | ----- | ----- | ----- | ------------------------------------ |
| J11  | VCC   | TX    | GND   | RX    | iLO debug UART, 3.3V, 115200 baud    |
| J33  | ?     | ?     | GND   | ?     | ?                                    |
| J4   | ?     | GND   | N.A.  | N.A.  | ?                                    |
| J2   | ?     | ?     | GND   | ?     | ?                                    |
| J1   | ?     | ?     | GND   | ?     | ?                                    |
| J34  | ?     | GND   | N.A.  | N.A.  | ?                                    |
| J18  | ?     | GND   | N.A.  | N.A.  | ?                                    |

J16 has more pads and was documented as a 38 pin MICTOR ARM ETM connector for
JTAG and trace debugging, by [Couzens and Held].



[NT5TU64M16DG-AC]: http://datasheet.elcodis.com/pdf2/118/85/1188556/nt5tu64m16dg-ac.pdf
[MX 29LV640DBTC-90G]: https://pdf1.alldatasheet.com/datasheet-pdf/view/267962/MCNIX/MX29LV640DBTC-90G.html
[88E1240-BAM1]: https://origin-www.marvell.com/transceivers/assets/88E1240_Technical_Product_Brief.pdf
[MX25L3205D]: http://pdf1.alldatasheet.com/datasheet-pdf/view/267908/MCNIX/MX25L3205D/+QW792-VKCMpZcPbNCSCCvGO+/datasheet.pdf
[CY62138FV30]: http://www.cypress.com/file/43771/download
[AT25DF641-S3H]: https://pdf1.alldatasheet.com/datasheet-pdf/view/256005/ATMEL/AT25DF641/+Q45Q2JVYSL.LcEXNCKM.EC+/datasheet.pdf
[AU6336]: https://pdf1.alldatasheet.com/datasheet-pdf/view/218984/ETC2/AU6336/+Q1J997VwSw9bDXzYvD+/datasheet.pdf
[PI3B3245LE]: https://pdf1.alldatasheet.com/datasheet-pdf/view/149271/PERICOM/PI3B3245LE/+0_8_8_VMDRpERP+uxzIww.hXS+/datasheet.pdf
[ADT7461A]: https://media.digikey.com/pdf/Data%20Sheets/ON%20Semiconductor%20PDFs/ADT7461A_Rev5(2009).pdf
[MAX8643A]: https://datasheets.maximintegrated.com/en/ds/MAX8643A.pdf
[SN74CBTLV3257]: https://www.ti.com/lit/ds/symlink/sn74cbtlv3257.pdf
[SN74LVC244A]: http://www.ti.com/lit/ds/symlink/sn74lvc244a.pdf
[MAX3243C]: https://datasheets.maximintegrated.com/en/ds/MAX3221-MAX3243.pdf
[Couzens and Held]: https://ecc2017.coreboot.org/uploads/talk/presentation/33/ecc2017_-_Porting_coreboot_to_the_HP_ProLiant_MicroServer_Gen8.pdf
