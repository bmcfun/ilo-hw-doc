# HPE ProLiant MicroServer Gen8

Part numbers: 775590-S01, 783958-S01, 783959-S01


## Chips

- [iLO4 Sabine](../../soc/sabine)


## External documentation

- [Porting coreboot to the HP ProLiant MicroServer Gen8](https://ecc2017.coreboot.org/uploads/talk/presentation/33/ecc2017_-_Porting_coreboot_to_the_HP_ProLiant_MicroServer_Gen8.pdf)
