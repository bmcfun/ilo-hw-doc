# GXP SoC

Part number: 815393-001

This SoC is used for iLO5.

Example chip markings:

```
88PAOSA1-BUY2
815393-001-B0
T8C45200.21JW
1631 C0E ES
TW 14
1 2 3 4 5
```
