# GLP-3 SoC

Part number: 531510-003

This SoC is used for iLO3 and for iLO4 on 8th-generation HP servers.

Example chip markings:

```
531510-003
1122LU7HB
● JAPAN
```


## Pinout

The BGA balls are numbered like this (top view):

```
      ____________________
 25  /                    \
     |  (hp) iLO 3        |
  .  |                    |
  .  |  531510-003        |
  .  |  ...               |
  4  |                    |
  3  |                    |
  2  |_                   |
  1  |/___________________/

     A B C D E ...      AE
```
