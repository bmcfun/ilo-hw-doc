# Sabine SoC

Part number: 610107-002

This SoC is used for iLO4.

According to the [iLO 4 Cryptographic Module Security Policy], "Sabine and
[GLP-4](../glp-4/index.md) are identical except that Sabine has a cache used by
drive arrays."

[iLO 4 Cryptographic Module Security Policy]: https://csrc.nist.gov/csrc/media/projects/cryptographic-module-validation-program/documents/security-policies/140sp2574.pdf
