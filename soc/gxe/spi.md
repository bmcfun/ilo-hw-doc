# SPI interfaces

There are two SPI controllers in the GXE SoC. The first (SPI0) can also be
accessed through the [memory-mapped flash](./mmflash.md) areas.

| Name  | Base         | [IRQ] | Connected to                                   |
| ----- | ------------ | ----- | ---------------------------------------------- |
| SPI0  | `0xc0000200` |    20 | SPI flash for firmware                         |
| SPI1  | `0xc0000280` |    21 |                                                |


## Theory of operation

The SPI controller operates in half-duplex, i.e. data can not be sent and
received at the same time. Special support for one command byte, up to four
address bytes, up to seven dummy bytes, is provided. After the SPI transfer,
the Chip Select pin (CS) can be automatically deasserted, or kept asserted.

SPI transfers are initiated via the status register. A SPI transfer goes
through the following phases:

- CS is asserted
- the [command](#0x05-command) byte is clocked out
- zero or more [address](#0x08-address) bytes are clocked out (the number of
  byte is configured in the [address length](#0x02-address-length) register)
- zero or more dummy bytes are clocked out
- the data contained in the [buffer](#0x40-buffer) is clocked out, or clocked
  in, depending on the configured direction mode
- CS is deasserted, unless bit 3 of the [status](#0x04-status)
  register was set

In a transfer that occurs after CS was kept asserted, the command byte, the
address bytes, and the dummy bytes are not clocked out. Rather, the transfer
begins with transferring data from or to the buffer.

The status register can be locked against accidental use. To unlock it, a
special unlock sequence has to be performed. Note that other register such as
the [flags](#0x00-flags) register are not protected by the locking machanism,
and it is still possible to render the SoC unbootable (until a power cycle) by
setting flags to zero.

The SPI controllers cannot be reset by software and keep their state across
[system resets](./misc.md#system-reset).


## Registers

| Offset | Type  | Description                                                  |
| ------ | ----- | ------------------------------------------------------------ |
| `0x00` | `u16` | [flags](#0x00-flags)                                         |
| `0x02` | `u8`  | [address length](#0x02-address-length)                       |
| `0x04` | `u8`  | [status](#0x04-status)                                       |
| `0x05` | `u8`  | [command](#0x05-command)                                     |
| `0x06` | `u8`  | [size](#0x06-size)                                           |
| `0x08` | `u32` | [address](#0x08-address)                                     |
| `0x0c` | `u8`  | [interrupt status](#0x0c-interrupt-status)                   |
| `0x0e` | `u8`  | [interrupt enable](#0x0e-interrupt-enable)                   |
| `0x40` |`64*u8`| [buffer](#0x40-buffer)                                       |


### 0x00: Flags 

The flags register holds some settings that control the behavior of the SPI
controller.

| Bits    |   Mask   | Description                                              |
| ------- | -------- | -------------------------------------------------------- |
|   [1:3] | `0x000e` | unknown                                                  |
|   [4:6] | `0x0070` | unknown                                                  |
|   [8:9] | `0x0300` | unknown                                                  |
| [11:13] | `0x3800` | number of dummy bytes                                    |
|    [14] | `0x4000` | unknown                                                  |

After power-on reset, the flags register holds the value `0x0b36`.

If bits `[1:3]` are set to `0` and the system is
[reset](./misc.md#system-reset), the system will not successfully boot anymore.
It is therefore advisable to keep the unknown fields as-is, and on change the
value of known fields.


### 0x02: Address length

This register contains the number of address bytes to be clocked out. Values
from `0` to `7` can be set, but values above `4` are treated as `0`.


### 0x04: Status

The status register is used to initiate SPI transfers and check for their
completion. It can be [locked and unlocked](#unlock-sequence) to prevent
accidental initiation of SPI transfers.

| Value  | Description                                                          |
| ------ | -------------------------------------------------------------------- |
| `0x01` | Go! (initiate a SPI transfer)                                        |
| `0x02` | the controller is currently busy with a SPI transfer                 |
| `0x04` | don't assert CS before this transaction, don't deassert CS afterward |
| `0x08` | direction mode: 1=write, 0=read                                      |
| `0x10` | unknown (related to locking)                                         |
| `0x20` | status register is locked                                            |


### 0x05: Command

The command register holds the first byte that is clocked out. This byte is
used as a command or opcode in protocols such as that of 25-series SPI NOR
flashes.

It is also used in the [unlock sequence](#unlock-sequence).


### 0x06: Size

This register holds the number of bytes to be transferred from or to the
buffer.


### 0x08: Address

This register holds the address which is clocked out after the command byte.
Although the register is in little-endian (least significant byte first), the
bytes are clocked out MSB-first.

Consider, for example, that 0x11223344 is stored in the address register.

| Address length | Address bytes clocked out                                    |
| -------------- | ------------------------------------------------------------ |
|              0 | (none)                                                       |
|              1 | `44`                                                         |
|              2 | `33 44`                                                      |
|              3 | `22 33 44`                                                   |
|              4 | `11 22 33 44`                                                |


### 0x0c: Interrupt status

The SPI controller supports two interrupt causes, which can be signalled to the
SoC's [interrupt controller](./intc.md). Bits set in the interrupt status
register indicate that an interrupt cause occured. 1-bits written to the
interrupt status register clear the corresponding interrupt status.

| Bit   | Description                                                           |
| ----- | --------------------------------------------------------------------- |
|    0  | SPI transfer was completed                                            |
|    1  | unknown                                                               |


### 0x0e: Interrupt enable

Bits set in this register indicate to the SPI controller that the corresponding
bits in the interrupt status register should be signalled to the interrupt
controller.


### 0x40: Buffer

The buffer holds data that will be clocked out (transferred to the attached SPI
device) in the next transfer, or have been clocked in (transferred from the
attached SPI device) in a previous transfer.


## Common usage patterns

### Unlock sequence

Before SPI transfers can be initiated, the SPI controller must be unlocked
using the following sequence:

- [command](#0x05-command) = 0x55
- [command](#0x05-command) = 0xaa
- Clear the lock bit in [status](#0x04-status), for example by writing 0x00

The SPI controller is locked after a power-on reset, but is not automatically
locked during runtime. To lock the controller:

- [status](#0x04-status) = 0x20

### Segmented messages

To provide flexibility in how SPI messages/commands are composed, it is
possible to split them into multiple transfers, that are executed in sequence.
For example, a SPI flash read command might be represented by the following
transfers:

| nr. | direction | size | data        | comments                               |
| --- | --------- | ---- | ----------- | -------------------------------------- |
|  -- | out       |    1 | `03`        | sent using the command byte register   |
|   1 | out       |    3 | `00 10 00`  | address: read from 0x1000              |
|   2 | in        |   64 | data @ 1000 | first 64 bytes                         |
|   3 | in        |   64 | data @ 1040 | second 64 bytes                        |
|   4 | in        |   64 | data @ 1080 | third 64 bytes                         |
|   5 | in        |   64 | data @ 10c0 | fourth 64 bytes                        |

To chain transfers in such a way it is important to avoid that CS is deasserted
after each transfer, because deasserting CS ends the message. This is achieved
by setting bit value `0x04` in the [status](#0x04-status) register when
initiating any of the involved transfers (1-5 in this example). After the
message is completed, CS can be manually deasserted by clearing bit value
`0x04` in the status register.

It should be noted that transfers with [size](#0x06-size) = 0 cannot be chained
in this way. If bit value `0x04` is set in the [status](0x04-status) register,
for such a transfer, it is ignored and CS is deasserted after the transfer
finishes.


[IRQ]: ./intc.md
