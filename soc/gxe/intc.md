# Interrupts

There are two almost identical interrupt controllers, that together manage 64
interrupt sources. They can signal interrupts to the ARM core as IRQs or FIQs.

| Base         | Size         | Description                                     |
| ------------ | ------------ | ----------------------------------------------- |
| `0xceff0000` | `0x00001000` | first interrupt controller, IRQs 0-31           |
| `0x80f00000` | `0x00001000` | second interrupt controller, IRQs 32-63         |


## Registers

| Offset   | Type       | Description                                           |
| -------- | ---------- | ----------------------------------------------------- |
| `0x0000` | `u32`      | IRQ status                                            |
| `0x0004` | `u32`      | FIQ status                                            |
| `0x0008` | `u32`      | raw interrupt status                                  |
| `0x000c` | `u32`      | signal as FIQ instead of IRQ                          |
| `0x0010` | `u32`      | unmask/enable an interrupt source                     |
| `0x0014` | `u32`      | mask/disable an interrupt source                      |
| `0x0018` | `u32`      | soft-raise                                            |
| `0x001c` | `u32`      | soft-lower                                            |
| `0x0030` | `u32`      | active IRQ, ACK (at `0xceff0030` for all 64 IRQs)     |
| `0x0100` | `u32` * 32 | interrupt identifiers                                 |
| `0x0200` | `u32` * 32 | must be `i+0x20` for every index i                    |

The first eight registers (up to `0x1c`) are bitmasks, where the bit index
identifies which interrupt source is meant.

How an interrupt is delivered:

- The interrupt line is raised, either by the interrupt source, or via the
  soft-raise register.
- The interrupt line must have be unmasked before the interrupt is delivered.
- If the bit corresponding to the interrupt line is set in the register at
  offset `0xc`, the interrupt is delivered as an FIQ.
- Otherwise, it is delivered as an IRQ, and the interrupt identifier appears in
  the register at `0x30`.
- To acknowledge the IRQ and allow further IRQs to be processed, a write to the
  register at `0x30` should be performed (the value doesn't seem to matter).


## Interrupt sources

| Number | Source                                                               |
| ------ | -------------------------------------------------------------------- |
|      0 | [timer](./misc.md#timers)                                            |
|     10 | [Ethernet MAC 0 @ 0xc0004000](./mac.md)                              |
|     11 | [Ethernet MAC 1 @ 0xc0005000](./mac.md)                              |
|     17 | [UART 0](./misc.md#uarts)                                            |
|     18 | [UART 1](./misc.md#uarts)                                            |
|     19 | [UART 2](./misc.md#uarts)                                            |
|     20 | [SPI0](./spi.md)                                                     |
|     21 | [SPI1](./spi.md)                                                     |
