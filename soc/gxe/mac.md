# Ethernet MAC

There are two Ethernet MACs in the GXE SoC. Each occupies 0x1000 (4k) bytes of
MMIO space, but decodes only the lower eight address lines, so the same
0x100-byte register window is repeated multiple times.

| Name  | Base         | [IRQ] | [Reset] | Typical usage                        |
| ----- | ------------ | ----- | ------- | ------------------------------------ |
| MAC0  | `0xc0004000` |    10 |     2   | Dedicated Ethernet PHY               |
| MAC1  | `0xc0005000` |    11 |     7   | [NC-SI] link to host NIC             |


## Theory of operation

Packet reception and transmission is based on a packet queue, a ring buffer of
packet descriptors accessed via DMA, per direction. Each packet descriptor
contains the address of a buffer to hold the packet data, and some metadata
such as the size, and whether or not the descriptor is ready to be processed by
the hardware.

Packets should be passed to the hardware as raw Ethernet frames including
padding (if necessary), so that they are at least 60 byte long. The frame check
sequence is added by the hardware, and brings the total frame size to at least
64 bytes, as required by the Ethernet standard.

In addition, the MAC supports [MDIO] via simple programmed IO, as explained
below.


## Registers

| Offset | Type  | Description                                                  |
| ------ | ----- | ------------------------------------------------------------ |
| `0x00` | `u32` | [flags](#0x00-flags)                                         |
| `0x04` | `u16` | [RX queue position](#0x04-rx-queue-position)                 |
| `0x06` | `u16` | [TX queue position](#0x06-tx-queue-position)                 |
| `0x08` | `u32` | [action](#0x08-action)                                       |
| `0x14` | `u32` | [queue sizes](#0x14-queue-sizes)                             |
| `0x18` | `u32` | [MAC address bytes 0/1](#0x18-0x20-mac-address)              |
| `0x1c` | `u32` | [MAC address bytes 2/3](#0x18-0x20-mac-address)              |
| `0x20` | `u32` | [MAC address bytes 4/5](#0x18-0x20-mac-address)              |
| `0x30` | `u32` | [interrupt](#0x30-interrupt)                                 |
| `0x4c` | `u32` | [DMA address of RX queue](#0x4c-address-of-rx-queue)         |
| `0x50` | `u32` | [DMA address of TX queue](#0x50-address-of-tx-queue)         |
| `0x80` | `u32` | [MDIO command](#0x80-mdio-command)                           |
| `0x84` | `u32` | [MDIO data](#0x84-mdio-data)                                 |


### 0x00: Flags 

The flags register allows software to enable/disable different features of the
Ethernet MAC, such as packet transmission (TX) and reception (RX).

| Flag value    | Description                                                   |
| ------------- | ------------------------------------------------------------- |
| `0x00000800`  | enable TX                                                     |
| `0x00001000`  | enable RX                                                     |


### 0x04: RX queue position

This register contains the index of the next descriptor in the RX queue to be
processed.


### 0x06: TX queue position

This register contains the index of the next descriptor in the TX queue to be
processed.


### 0x08: Action

Writing `1` to this register causes the MAC to start processing packets and
queue descriptors.


### 0x14: Queue sizes

This register lets software configure the size of the RX and TX queues.
The register values are computed like this:

```
reg = ((Number of descriptors) / 4) - 1
```

| Bits    | Description                                                         |
| ------- | ------------------------------------------------------------------- |
| [31:24] | TX queue size                                                       |
| [23:16] | RX queue size                                                       |

Queue sizes that are not powers of two should be avoided, as they don't seem to
work. Values less than four, or non-multiples of four are not representable.


### 0x18-0x20: MAC address

The MAC hardware contains a MAC address filter for incoming (received) packets.

| Register | Type  | Value                                                      |
| -------- | ----- | ---------------------------------------------------------- |
|  `0x18`  | `u32` | byte0 << 8 | byte1                                         |
|  `0x1c`  | `u32` | byte2 << 8 | byte3                                         |
|  `0x20`  | `u32` | byte4 << 8 | byte5                                         |


### 0x30: Interrupt

The interrupt register combines interrupt status interrupt masking, for the
different interrupt causes supported by the Ethernet MAC.

There are three kinds of bits:

- **Interrupt status bits**: This kind of bit reads 1 if an interrupt is
  present, 0 otherwise. Interrupt status is cleared by writing a 1.
- **Interrupt enable bits**: These bits control whether interrupt status is
  singalled to the [interrupt controller](./intc.md) (1=enabled, 0=disabled).
  After reset, all interrupts are disabled; otherwise, the previously written
  value can be read back.
- **Interrupt soft-raise bits**: These bits allow software to raise individual
  interrupts by writing 1. They always read 0.

| Bit   | Description                                                           |
| ----- | --------------------------------------------------------------------- |
|    0  | RX interrupt status: one or more RX queue descriptors were processed  |
|    1  | RX interrupt enable                                                   |
|    2  | TX interrupt status: one or more TX queue descriptors were processed  |
|    3  | TX interrupt enable                                                   |
|    4  | unknown interrupt status                                              |
|    5  | interrupt enable for bit 4                                            |
|    6  | --                                                                    |
|    7  | --                                                                    |
|    8  | soft-raise for bit 0 (RX)                                             |
|    9  | soft-raise for bit 2 (TX)                                             |
|   10  | soft-raise for bit 4                                                  |
|   11  | --                                                                    |
|   12  | unknown interrupt status                                              |
|   13  | interrupt enable for bit 12                                           |
|   14  | soft-raise for bit 12                                                 |
|   15  | --                                                                    |


### 0x4c: Address of RX queue

This register holds the base address of the RX queue. High mirrors of the RAM
available to the CPU, such as the one at `0x40000000`, should not be used.
Instead, always use the lowest mirror at `0x00000000`.


### 0x50: Address of TX queue

This register holds the base address of the TX queue. Like with `0x4c`, don't
use high RAM mirrors.


### 0x80: MDIO command

This register is used to configure device and register addresses, and initiate
transfers, on the MDIO bus connected to the MAC.

| Bits    | Description                                                         |
| ------- | ------------------------------------------------------------------- |
|   [4:0] | Register address                                                    |
|     [8] | Read (set to 1 before read transfers)                               |
|     [9] | Busy                                                                |
| [20:16] | PHY address                                                         |


### 0x84: MDIO data

This register contains the data read or writted over the MDIO bus.


## Packet queues

Packet descriptors are 16 bytes long:

| Offset | Type | Description                                                   |
| ------ | ---- | ------------------------------------------------------------- |
|  `0x0` |  u32 | DMA Address of packet buffer (should be two-byte aligned)     |
|  `0x4` |  u16 | flags                                                         |
|  `0x6` |  u16 | length of packet                                              |
|  `0x8` |  u32 | unknown, should be zero                                       |
|  `0xc` |  u32 | can be used by software for any purpose                       |


### Flags

| Value  | RX flags                         | TX flags                          |
| ------ | -------------------------------- | --------------------------------- |
|`0x8000`| pending                          | pending                           |


### Processing one or more packets

- Find the next free packet descriptor
- Set the DMA address and length; set the flags field to `0x8000`
- Set the other two fields to zero
- Repeat for more packet descriptors if desired
- Trigger the state machine via the action register (`0x08`)
- When an interrupt is signaled, check the interrupt status register for RX
  queue or TX queue interrupts.
- Walk over the packet descriptors that are not active anymore
  (`flags & 0x8000 == 0`); check the flags field in each packet descriptor to
  detect errors. For RX descriptors, the length of the received packet can be
  read from the length field.


## MDIO

To read an MDIO register:

```
command register = phy << 16 | reg
command register = phy << 16 | reg | 0x100
command register = phy << 16 | reg | 0x300
poll the command register until 0x200 (the busy flag) is clear
read the data value from the data register
```


To write an MDIO register:

```
command register = phy << 16 | reg
data register    = data
command register = phy << 16 | reg | 0x200
poll the command register until 0x200 (the busy flag) is clear
```



[IRQ]: ./intc.md
[Reset]: ./misc.md#reset-controller
[NC-SI]: https://en.wikipedia.org/wiki/NC-SI
[MDIO]: https://en.wikipedia.org/wiki/MDIO
