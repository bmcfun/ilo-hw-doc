# GXE SoC

Part numbers: 438893-502, 438893-503

Typical hardware revision: 0x40050

This SoC is used for iLO3.

Example chip markings:

```
438893-503
1031LU7H9
● JAPAN
```

## CPU

GXE contains a [ARM926EJ-S] revision 5 clocked at 250MHz, with
16 KiB each of [DTCM and ITCM].


## Peripherals

- [SPI](spi.md)
- [I²C](i2c.md)
- [DDR2 RAM](ddr.md)


## Memory map

| Base         | Size         | Description                                     |
| ------------ | ------------ | ----------------------------------------------- |
| `0x80f00000` | `0x00001000` | [2nd interrupt controller](./intc.md)           |
| `0xc0000000` | `0x00000200` | [MISC](./misc.md)                               |
| `0xc0000200` | `0x00000080` | [SPI0](./spi.md)                                |
| `0xc0000280` | `0x00000080` | [SPI1](./spi.md)                                |
| `0xc0004000` | `0x00001000` | [Ethernet MAC 0](./mac.md)                      |
| `0xc0005000` | `0x00001000` | [Ethernet MAC 1](./mac.md)                      |
| `0xceff0000` | `0x00001000` | [1st interrupt controller](./intc.md)           |
| `0xd1000000` | `0x00001000` | EXPBUS/CPLD                                     |
| `0xf8000000` | `0x02000000` | [memory-mapped flash](./mmflash.md)             |
| `0xffff0000` | `0x00010000` | [bootblock](./mmflash.md)                       |



[ARM926EJ-S]: http://infocenter.arm.com/help/topic/com.arm.doc.ddi0198e/DDI0198E_arm926ejs_r0p5_trm.pdf
[DTCM and ITCM]: https://www.kernel.org/doc/html/latest/arm/tcm.html
