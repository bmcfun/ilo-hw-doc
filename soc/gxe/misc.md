# MISC

The register block at `0xc0000000` is a loose collection of miscellaneous
hardware functions.


## Register overview

| Offset   | Size      | Description                                            |
| -------- | --------- | ------------------------------------------------------ |
| `0x0000` | `0x0001`  | [system reset](#system-reset)                          |
| `0x00a0` | `0x0004`  | [hardware revision](#hardware-revision)                |
| `0x0080` | `0x0020`  | [timers](#timers)                                      |
| `0x00c0` | `0x0002`  | [reset controller](#reset-controller)                  |
| `0x00e0` | `0x0008`  | [8250-style UART 0](#uarts)                            |
| `0x00e8` | `0x0008`  | [8250-style UART 1](#uarts)                            |
| `0x00f0` | `0x0008`  | [8250-style UART 2](#uarts)                            |


## System reset

| Offset   | Type      | Description                                            |
| -------- | --------- | ------------------------------------------------------ |
| `0x0000` | `u8`      | Write `1` here to reset the whole SoC                  |

32-bit accesses to the system reset register also work.


## Hardware revision

| Offset   | Type      | Description                                            |
| -------- | --------- | ------------------------------------------------------ |
| `0x00a0` | `u32`     | hardware revision, e.g. 0x40050 for GXE                |


## Timers

| Offset   | Type      | Description                                            |
| -------- | --------- | ------------------------------------------------------ |
| `0x0080` | `u32`     | countdown value, counting down at 1 MHz                |
| `0x0088` | `u32`     | fast (125 MHz) timestamp counter, low 32 bits          |
| `0x008c` | `u32`     | fast (125 MHz) timestamp counter, high 32 bits         |
| `0x0094` | `u32`     | flags                                                  |
| `0x0098` | `u32`     | slow (1 MHz) timestamp counter, low 32 bits            |
| `0x009c` | `u32`     | slow (1 MHz) timestamp counter, high 32 bits           |

The timestamp counters continually count up, at the rate of 125 MHz or 1 MHz
respectively.

The timer hardware can generate interrupts on [IRQ 0](./intc.md). When the
timer is idle, a new countdown value can be written to the countdown register;
it starts counting down once oneshot mode or periodic mode is entered, by
writing `1` or `3` respectively to the flags register.

In oneshot mode, the countdown counts down to zero, then the interrupt is
raised, and the countdown is reset to the initial value.

In periodic mode, the countdown is reset to the initial value once it reaches
zero, but also keeps counting down, so that interrupts are raised repeatedly.

Interrupt presence is shown in bit 7 (value `0x80`) of the flag register and
can be cleared by writing a value in which that bit is set.


## Reset controller

| Offset   | Type      | Description                                            |
| -------- | --------- | ------------------------------------------------------ |
| `0x00c0` | `u16`     | Reset lines (32-bit access also works)                 |

The 16-bit register at `0xc00000c0` controls 16 reset lines. After a 1-bit is
written, the corresponding peripheral is kept in reset until a 0-bit is written
again. In the list below, reset lines are identified by their bit number; for
example, line 2 corresponds to a bit mask of `1 << 2 == 0x0004`.

| Line  | Device                                                                |
| ----- | --------------------------------------------------------------------- |
|    2  | [Ethernet MAC 0 @ 0xc0004000](./mac.md)                               |
|    7  | [Ethernet MAC 1 @ 0xc0005000](./mac.md)                               |


## UARTs

| Offset   | IRQ | Description                                                  |
| -------- | --- | ------------------------------------------------------------ |
| `0x00e0` |  17 | 8-bit 8250-style UART 0                                      |
| `0x00e8` |  18 | 8-bit 8250-style UART 1                                      |
| `0x00f0` |  19 | 8-bit 8250-style UART 2; usually used for debug output       |
