# Memory-mapped flash

SPI flash connected to [SPI0](./spi.md) is mapped to the following ranges of
physical memory:

| Base         | Size         | Description                                     |
| ------------ | ------------ | ----------------------------------------------- |
| `0xf8000000` | `0x02000000` | 32 MiB window to cover the full flash           |
| `0xffff0000` | `0x00010000` | bootblock (last 64KiB of the flash)             |


## Theory of operation

Read requests to the memory-mapped flash range are translated by the SPI
controller to read commands for the attached SPI flash.

In the physical address range from `0xf8000000` to `0xf9ffffff`, only the lower
24 address bits are decoded, resulting in SPI flash offsets from `0x000000` to
`0xffffff`. The next higher address bit is ignored, so that the flash contents
can be seen twice (or even more often if the flash itself ignores address bits).

The physical address range from `0xffff0000` to `0xffffffff` (the last 64 KiB
of physical address space) corresponds to SPI flash offsets from `0xff0000` to
`0xffffff` (the last 64 KiB of the SPI flash). Because the
[ARM CPU](./index.md#cpu) begins execution with its [vector table] at
`0xffff0000` the first stage of boot code should be placed into the last 64 KiB
of the SPI flash.


### Effects on the SPI bus

After a read request is completed, the chip select (CS#) stays asserted (at
0V), so that further read requests (at subsequent addresses) can be completed
by continuing the same SPI flash read command.


TODO: Opcodes, address length, dummy bytes, etc.


[vector table]: http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.dui0471m/pge1358787016886.html
