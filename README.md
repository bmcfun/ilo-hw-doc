# The ilo-hw-doc project

## Independent HP iLO3/4/5 hardware documentation

This repo contains information about the inner workings of the hardware used to
implement [HP's integrated Lights Out][iLO] remote management solution.


### Legal information

The documentation in this repository is not provided or endorsed by HP,
but written by independent researchers.
<!-- TODO: replace this by a lawyer-approved disclaimer -->

All files in this repo are covered by the [CC-BY-4.0][CC-BY-4.0] license.



## SoCs

- [GXE, part number 438893-503](soc/gxe/index.md)
- [GLP-3, part number 531510-003](soc/glp-3/index.md)
- [Sabine, part number 610107-002](soc/sabine/index.md)
- [GLP-4, part number 531510-004](soc/glp-4/index.md)
- [GXP, part number 815393-001](soc/gxp/index.md)


## Boards

- [HP ProLiant DL585 G7 System Peripheral Interface Board, 591199-001](board/dl585-g7-spi/index.md)
- [HPE ProLiant MicroServer Gen8](board/microserver-g8/index.md)


## External documentation

- Airbus Seclab: Subverting your server through its BMC: the HPE iLO4 case
  ([slides][airbus-slides], [paper][airbus-paper])
- [FIPS 140-2 Non-Proprietary Security Policy for iLO3][FIPS140-ilo3]
- [FIPS 140-2 Non-Proprietary Security Policy for iLO4][FIPS140-ilo4]
- [Airbus Seclab: Turning your BMC into a revolving door][rdoor] ([slides][rdoor-slides])


[iLO]: https://en.wikipedia.org/wiki/HP_Integrated_Lights-Out
[CC-BY-4.0]: https://creativecommons.org/licenses/by/4.0/legalcode
[airbus-slides]: https://airbus-seclab.github.io/ilo/RECONBRX2018-Slides-Subverting_your_server_through_its_BMC_the_HPE_iLO4_case-perigaud-gazet-czarny.pdf
[airbus-paper]: https://airbus-seclab.github.io/ilo/SSTIC2018-Article-subverting_your_server_through_its_bmc_the_hpe_ilo4_case-gazet_perigaud_czarny.pdf
[FIPS140-ilo3]: https://csrc.nist.gov/csrc/media/projects/cryptographic-module-validation-program/documents/security-policies/140sp2173.pdf
[FIPS140-ilo4]: https://csrc.nist.gov/csrc/media/projects/cryptographic-module-validation-program/documents/security-policies/140sp2574.pdf
[rdoor]: https://2018.zeronights.ru/en/reports/turning-your-bmc-into-a-revolving-door/
[rdoor-slides]: https://airbus-seclab.github.io/ilo/ZERONIGHTS2018-Slides-EN-Turning_your_BMC_into_a_revolving_door-perigaud-gazet-czarny.pdf
